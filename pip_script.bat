@echo off
echo "Installing tensorflow"
pip install tensorflow
echo "Installing pygame"
pip install pygame
echo "Installing autopytoexe"
pip install auto-py-to-exe
echo "Installing pycryptodome"
pip install pycryptodome
echo "Installing cryptography"
pip install cryptography
echo "Installing matplotlib"
pip install matplotlib
echo "Installing pandas"
pip install pandas
echo "Installing django"
pip install django
pause
