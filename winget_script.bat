@echo off

winget install BraveSoftware.Brave-Browser

winget install KDE.KDEConnect

winget install SumatraPDF.SumatraPDF

winget install WhatsApp.WhatsApp

winget install Telegram.TelegramDesktop

mkdir "D:\Program Files\GIMP"

winget install GIMP.GIMP --location "D:\Program Files\GIMP"

pause